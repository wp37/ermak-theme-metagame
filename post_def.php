
			<div>
				
					<?php 
						$youtube_id 		= get_post_meta($post->ID, 'youtube_id', true);		
						$slide_title		= get_post_meta($post->ID, 'slide_title', true);
						$slide_1 			= get_post_meta($post->ID, 'slide_1', true);
						$slide_2 			= get_post_meta($post->ID, 'slide_2', true);
						$slide_3 			= get_post_meta($post->ID, 'slide_3', true);
						$slide_4 			= get_post_meta($post->ID, 'slide_4', true);	
						$tax				= ( has_term( '', 'location' ) ) ? get_the_term_list( $post->ID, 'location', '<i class="fa fa-sun-o"></i> ', ',', '' ) : "";
					?>
					
				
				<div class="postdate" width=100%>
					<div>
						<span style=""><i class="fa fa-calendar-o"> </i> <?php the_time('j M, Y')?></span> 
						<i class="fa fa-folder-open"></i> <?php the_category(', ') ?> 
						<?php echo $tax; ?>
						<i class="fa fa-comment"></i> <?php comments_popup_link('Нет комментариев', '1 комментарий', '% коммент.'); ?> 
						<?php if (current_user_can('edit_post', $post->ID)) { ?>
							<i class="fa fa-rocket"></i> <?php edit_post_link('Правка', '', ''); } ?> 
					</div>
				</div>		
				<div class='autor-entry'>
					<div class='avatar-entry'>
						<?php echo get_avatar(); ?>	
					</div>
					<div class='nick-entry'>
						<?php the_author() ?> 
					</div>	
									
				</div>
				<div class='autor-entry'>
					
					<h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Постоянная ссылка для <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<?php //the_content('Читать далее &raquo;');  ?>
					
						<?php if($youtube_id)
						{
							echo '<div class="archive-post-video">';
							echo  do_shortcode('[video_lightbox_youtube video_id="'.$youtube_id.'" width="640" height="480" auto_thumb="1"]');
							echo '</div>';
						}
						else if($slide_1)
						{
							?><div style="display:block;min-height:110px; margin:0 0 10px 0 "><?php
							echo '<a href='.$slide_1.' rel="lightbox['.$slide_1.']">';
							if($slide_title)
								echo '<div>'.$slide_title.'</div>';
							//echo preg_replace ('/width="500" height="375"/', 'width="320" height="240"', $yandex_photo_id);	
							echo '<div class="slidshow_thumb"><img src='.$slide_1.' style="display: inline"  title="'.$slide_title.'"></div></a>';		
							if($slide_2)
								echo '<a href='.$slide_2.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb"><img src='.$slide_2.' style="display: inline"  title="'.$slide_title.'"></div></a>';		
							if($slide_3)
								echo '<a href='.$slide_3.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb"><img src='.$slide_3.' style="display: inline"  title="'.$slide_title.'"></div></a>';		
							if($slide_4)
								echo '<a href='.$slide_4.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb"><img src='.$slide_4.' style="display: inline"  title="'.$slide_title.'"></div></a>';	
							?></div><?php
						}
						$add_comment	= "оставить комментарий";
						?>	
					
					<p>
					<span  class="wp-button">
						<?php comments_popup_link($add_comment.' (Нет комментариев)', $add_comment.' (1 комментарий)', $add_comment.' (% коммент.)');?>
					</span >
					</p>
				</div>
			</div>
