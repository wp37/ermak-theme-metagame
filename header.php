<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed|Open+Sans+Condensed:300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oranienbaum&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Stalinist+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/print.css" type="text/css" media="print" />
<!--[if IE]><link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/ie.css" type="text/css" media="screen, projection"><![endif]-->
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/secondeffect.css" type="text/css" media="screen"/>

<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_enqueue_script("jquery"); ?>
<?php echo get_theme_option("head") . "\n";  wp_head(); ?>


<script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.4.4.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.cycle.all.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/menu/superfish.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/menu/custom.js"></script>
</head>
<body <?php body_class(); ?>>
	<!--LiveInternet counter--><script type="text/javascript"><!--
			new Image().src = "//counter.yadro.ru/hit?r"+
			escape(document.referrer)+((typeof(screen)=="undefined")?"":
			";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
			screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
			";"+Math.random();//-->
	</script><!--/LiveInternet-->
	<?php do_action('body_start'); ?>
	<div id="wrapper"><div id="wrapper-bg"><div id="wrapper-bg2">

		<div id="outer-wrapper" class="outer-wrapper">  
           
            
				<div id="header" class="outer" style="overflow:hidden;">
					<div style="border:0; position:relative; width:49%; float:left; margin-right:10px;">
						<?php
						$get_logo_image = get_theme_option('logo');
						if($get_logo_image != '') {
							?>
							<a href="<?php bloginfo('url'); ?>">
								<img src="<?php echo $get_logo_image; ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>" class="logoimg" />
							</a>
							<?php
						} 
						else 
						{
							?>
							<div style="">
								<div style='padding:11px 3px 0 0; border-bottom:0!important;'>
									<h1 style='border-bottom:0!important;'><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h1>
								</div>
								<div style='padding:0px 0px 0 20px; float:left;'>
									<h2><?php bloginfo('description'); ?></h2>
								</div>
							</div>
							<?php
						}
						?>
						
					</div>
					
					<div class="header-sidebar-cont"> 					 
						<div class="header-sidebar">
							<?php dynamic_sidebar('head_sidebar');?>  
						</div>		  
					</div>
				</div>
			
			<div class="outer">
				   <div class="navi-primary-container">
					<?php
                    if(function_exists('wp_nav_menu')) {
                       wp_nav_menu( 'theme_location=menu_1&menu_class=navi-primary navis&container=&fallback_cb=menu_1_default');
                    } else {
                       menu_1_default();
                    }
                    
                    function menu_1_default()
                    {
                        ?>
                        <ul class="navis navi-primary">
    						<li <?php if(is_home()) { ?> class="current_page_item" <?php } ?>><a href="<?php echo get_option('home'); ?>/">Главная</a></li>
    						<?php wp_list_pages('sort_column=menu_order&title_li=' ); ?>
    					</ul>
                        <?php
                    }
                    
                ?>
                    </div>
 <div id="topsearch" style="margin-top:6px;"> 
    		<?php get_search_form(); ?> 
    	</div>
			</div>
			
<?php
	
	
	
?>