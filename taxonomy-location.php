<?php 
		get_header(); 	
		?>
		
<div class="outer" id="contentwrap">
		
	
	<?php get_sidebars('left'); ?>
	<div class="postcont">
		<div id="content">	
		<?php
			$cur_term		= get_term_by("name", single_term_title("",0), "location");		
			?><h4 class="pagetitle"><?php echo apply_filters("smc_location_archive_title", $cur_term->name, $cur_term); ?></h4><?php			
		
		//global $wp_query;
		//var_dump($wp_query);		
	
		if (have_posts()) : 
		 ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php if( get_post_type() != "post") 
			{
				continue;
			}?>
			<div <?php post_class() ?>>
				<?php get_template_part( "post"); ?>
			</div>

		<?php endwhile; ?>
		
		<div class="navigation">
		<?php if (function_exists("pagination")) {
			pagination($additional_loop->max_num_pages);
} 		?>

		</div>
	<?php else :

		echo("<h2>Ничего не найдено.</h2>");
		get_search_form();
		get_template_part( 'content', '' );
		
		?>
		
	<?php endif; ?>
			</div>
		
			</div>

<?php get_sidebars('right'); ?>
	</div>
<?php get_footer(); ?>
