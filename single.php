<?php get_header(); 
$description_title 			= get_post_meta($post->ID, 'description_title', true);
$description	 			= get_post_meta($post->ID, 'description', 		true);
?>
	<div class="outer" id="contentwrap">
	
            <?php get_sidebars('left'); ?>
			<div class="postcont">
				
				<div id="content">	
					<?php if (have_posts()) : ?>	
						<?php while (have_posts()) : the_post(); 						
						$tax	= ( has_term( '', 'location' ) ) ? get_the_term_list( $post->ID, 'location', '<i class="fa fa-sun-o"></i> ', ',', '' ) : "";
												
						?>
						
						<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
							<div>
								<h2 class="page_title"><?php the_title(); ?></h2>	
							</div>
							<div class="entry" style='padding:"20px;"'>
								<div>
								   <?php 
										if($youtube_id)
										{
											echo'<iframe width=100% height="400" src="//www.youtube.com/embed/'.$youtube_id.'" frameborder="0" allowfullscreen></iframe>';
										}
										else if($yandex_photo_id)
										{
											echo $yandex_photo_id;
											//echo '<object width="500" height="375"><param name="bgcolor" value="#000000"/><param name="movie" value="http://fotki.yandex.ru/swf/slideshow"/><param name="FlashVars" value="author=genag1&amp;effects=1&amp;host_img=img-fotki.yandex.ru&amp;host_xml=fotki.yandex.ru&amp;mode=album&amp;id=$yandex_photo_id"/><param name="allowFullScreen" value="true"/><param name="quality" value="high"/><embed src="http://fotki.yandex.ru/swf/slideshow" flashvars="author=genag1&amp;effects=1&amp;host_img=img-fotki.yandex.ru&amp;host_xml=fotki.yandex.ru&amp;mode=album&amp;id=$yandex_photo_id" allowfullscreen="true" quality="high" width="500" height="375" bgcolor="#000000" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"/></object>';
										}
									?>
								</div>
								<!--div class="postdate-single">Опубликовано  автором <?php the_author() ?> </div>
								<div class="postdate-single-2"><img src="<?php bloginfo('template_url'); ?>/images/folder.png" /> Опубликовано в <?php the_category(', ') ?> <?php if(get_the_tags()) { ?> <img src="<?php bloginfo('template_url'); ?>/images/tag.png" /> <?php  the_tags('Метки: ', ', '); } ?> <?php the_time('F j, Y') ?></div-->
								<div class='location'>
									<?php echo $tax; ?>
								</div>
								<div class="postdate">
									<div>
										<i class="fa fa-calendar-o"></i>&nbsp;
										<?php the_time('j M, Y')  ?>&nbsp;&nbsp;										
										<i class="fa fa-folder-open"></i>&nbsp;
										<?php the_category(', ') ?>&nbsp;&nbsp;
										<i class="fa fa-comment"></i>&nbsp;
										<?php comments_popup_link('Нет комментариев', '1 комментарий', '% коммент.'); ?>&nbsp;
										<?php if (current_user_can('edit_post', $post->ID)) { ?>&nbsp;
											<i class="fa fa-rocket"></i>&nbsp;
											<?php edit_post_link('Правка', '', ''); } ?>&nbsp;
									</div>
								</div>	
								<?php if($slide_1)
										{
											echo '<div>';
											echo '<a href='.$slide_1.' rel="lightbox['.$slide_1.']">';
											if($slide_title)
												echo '<div>'.$slide_title.'</div>';
											//echo preg_replace ('/width="500" height="375"/', 'width="320" height="240"', $yandex_photo_id);	
											echo '<div class="slidshow_thumb_post"><img src='.$slide_1.' style="display: inline"  title="'.$slide_title.'"></div></a>';		
											if($slide_2)
												echo '<a href='.$slide_2.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb_post"><img src='.$slide_2.' style="display: inline"  title="'.$slide_title.'"></div></a>';		
											if($slide_3)
												echo '<a href='.$slide_3.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb_post"><img src='.$slide_3.' style="display: inline"  title="'.$slide_title.'"></div></a>';		
											if($slide_4)
												echo '<a href='.$slide_4.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb_post"><img src='.$slide_4.' style="display: inline"  title="'.$slide_title.'"></div></a>';	
											echo '</div>';
										} ?>
								<?php the_content('Читать далее &raquo;'); ?>
								<?php if(function_exists('get_shr_like_buttonset')) { get_shr_like_buttonset('Bottom'); } ?>
								<?php wp_link_pages(array('before' => '<p><strong>Страницы:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
							</div>
							
						
							
							
							<?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
								// Both Comments and Pings are open ?>
								Вы можете <a href="#respond">оставить комментарий</a>, или <a href="<?php trackback_url(); ?>" rel="trackback"> ссылку</a> на Ваш сайт.
	
							<?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
								// Only Pings are Open ?>
								Комментирование на данный момент запрещено, но Вы можете оставить <a href="<?php trackback_url(); ?> " rel="trackback">ссылку</a> на Ваш сайт.
	
							<?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
								// Comments are open, Pings are not ?>
								Вы можете пропустить чтение записи и оставить комментарий. Размещение ссылок запрещено.
	
							<?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
								// Neither Comments, nor Pings are open ?>
								Комментирование и размещение ссылок запрещено.
	
							<?php } edit_post_link('Правка','','.'); ?>
						</div><!--/post-<?php the_ID(); ?>-->
						
				<?php comments_template(); ?>
				
				<?php endwhile; ?>
			
				<?php endif; ?>
			</div>
			</div>
		<?php get_sidebars('right'); ?>
	</div>
<?php get_footer(); ?>


