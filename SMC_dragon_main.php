<?php
/*
Template Name: Main User Panel Template
*/
?>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) { echo '<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">'; } ?>
<?php if( function_exists('wp_is_mobile') && wp_is_mobile() ) { ?>
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=yes">
<meta name="HandheldFriendly" content="true">
<?php } ?>
<link rel="profile" href="http://gmpg.org/xfn/11">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<!-- STYLESHEET INIT -->

<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css" />
<!-- favicon.ico location -->
<?php
$get_fav_icon =  get_theme_option('fav_icon');
if( $get_fav_icon ) { ?><link rel="icon" href="<?php echo $get_fav_icon; ?>" type="images/x-icon" /><?php } ?>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php $header_code = get_theme_option('header_code'); echo stripcslashes(do_shortcode($header_code)); ?>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> id="custom">
	<div class="outer" id="contentwrap">
		<div>
			<div>
				<ul>
					<?php 
						if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar( 'Right Sidebar' ) ) : ?>			
					
					<?php endif; ?>
				</ul>        
			</div>		
		</div>
		<div class="postcont" style="margin:5px 20px; min-height:400px;">
			<div id="content">	
				<?php echo do_shortcode( '[smc_dragon_user_panel]' );?>	
			</div>
		</div>
	
	</div>
<?php
	wp_footer();
	echo get_theme_option("footer")  . "\n";
?>
</body>
</html>
