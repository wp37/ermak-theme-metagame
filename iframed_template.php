<?php
/*
Template Name: I-framed Template
*/
?>
<?php get_header(); 
	$youtube_id 		= get_post_meta($post->ID, 'youtube_id', true);
	$yandex_photo_id 	= get_post_meta($post->ID, 'yandex_photo_id', true);
	$slide_title		= get_post_meta($post->ID, 'slide_title', true);
	$slide_1 			= get_post_meta($post->ID, 'slide_1', true);
	$slide_2 			= get_post_meta($post->ID, 'slide_2', true);
	$slide_3 			= get_post_meta($post->ID, 'slide_3', true);
	$slide_4 			= get_post_meta($post->ID, 'slide_4', true);
	$description_title	= get_post_meta($post->ID, 'description_title', true);
	$description		= get_post_meta($post->ID, 'description', true);
?>
<div class="outer" id="contentwrap">
	
   <div class='featured-post-slides'>
		<?php 
			if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) 
			{ 
				the_post_thumbnail("full", array("class" => "alignleft post_thumbnail")); 
			}
			else
			{
				echo $default_thumbail;
			} 
		?>
		<div class="featured-post-content">
			<span class="featured-post-title">
				<h3 style="color:#FFF;"><?php echo $description_title; ?></h3>				
				<?php echo $description;?>	
			</span>			
		</div>
	</div>
	<div class='sidebar sidebar-left' style="float:left;">.</div>
	<div class="postcont">
		
		<div id="content">	
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="post" id="post-<?php the_ID(); ?>">
				<div class="entry">
					
					<h2 class="page_title"><?php the_title(); ?></h2>
					<?php the_content('<p class="serif">Читать далее &raquo;</p>'); ?>
					<?php wp_link_pages(array('before' => '<p><strong>Страницы:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
					
	
				</div>
			</div>
			<?php endwhile; endif; ?>
		<?php edit_post_link('Правка', '<p>', '</p>'); ?>
		</div>
	</div>
	

<?php //get_sidebars('right'); ?>

</div>
<?php get_footer(); ?>