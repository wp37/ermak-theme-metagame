<?php get_header();  
$description_title 			= get_post_meta($post->ID, 'description_title', true);
$description	 			= get_post_meta($post->ID, 'description', 		true);
?>
<div class="outer" id="contentwrap">
	
	
    <?php get_sidebars('left'); ?>	
	<div class="postcont">
		<div id="content">	
						
			<?php 
				if (have_posts()) 			: while (have_posts()) : the_post();
			?>
			<div class="post" id="post-<?php the_ID(); ?>">
				<div class="entry">
					
			<h2 class="page_title"><?php the_title(); ?></h2>
					<?php the_content('<p class="serif">Читать далее &raquo;</p>'); ?>
	
					<?php wp_link_pages(array('before' => '<p><strong>Страницы:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
	
				</div>
			</div>
			<?php endwhile; endif; ?>
		<?php edit_post_link('Правка', '<p>', '</p>'); ?>
		</div>
	</div>
	

<?php get_sidebars('right'); ?>

</div>
<?php get_footer(); ?>