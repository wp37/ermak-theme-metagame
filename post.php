<style type="text/css">		
	.circle_1, .circle_1 a
	{	
		position:relative;		
		color:#FFFFFF;
		margin:0 5px 5px 0;
		background: #3686CB;
		width:40px;
		height:40px;
		-moz-border-radius: 20px;
		-webkit-border-radius: 20px;
		border-radius: 20px;
		text-align:center;
		float:left;
		size:9px;
	}
	.circle_1:hover
	{
		background: #215584;
		color:#FFF;
	}
	.circle_1 a:active
	{
		color:#000;
	}
	.circle_1:active
	{
		background: #3686CB;
	}
	.picto, .picto a
	{
		background:none!important;
		position:absolute; 
		top:14px;
		#width:40px;
		height:40px;
	}
	
	.post-attribute, .post-atribute-autor
	{
		border-left:3px solid #DDD;		
		color:#AAA;
		font-size:10px;
		padding-left:5px;
		margin-bottom:3px;
		#line-height:2pt;
		
	}
	.post-attribute a
	{
		color:#AAA;
	}
	.post-attribute a:href
	{
		color:#666;
	}
	.post-atribute-autor
	{
		font-family: 'Ubuntu Condensed', serif;
		font-size:17px;
		font-weight: 900;
		text-overflow:hidden;
		border-bottom:1px dotted #AAA;
		#padding-bottom:9px;
		#display:block;
		width:100%;
	}
	.add-comment
	{
		margin:1px;
		margin-top:10px;
		background:#215584;
		padding:15px 15px;
		color:rgba(255,255,255,0.5);
	}
	.add-comment a
	{
		color:rgba(255,255,255,0.75);
	}
	.add-comment:hover
	{
		color:#FFF;
		padding:15px 15px;
	}
	.add-comment:active
	{
		background: #3686CB;
		padding:14px 15px 16px 15px;
	}
</style>
<?php	
	$tax				= ( has_term( '', 'location' ) ) ? get_the_term_list( $post->ID, 'location', '<i class="fa fa-sun-o"></i> ', ',', '' ) : "";
?>
	<div style="position:relative; border-top:1px dotted #AAA; padding-top:3px; padding-bottom:0;">
		<table cellspacing="0" cellpadding="0" rules="rows" border="0">
			<tr>
				<td valign='top' height='50'>
					<div style="vertical-align:top; float:left; cpadding:0; padding-right:10px;width:140px; border-right:1px dotted #EEE;">
					<?php $my_post = get_post();?>
						<div style="position:relative; display:block;">
							<div class="circle_1  hint  hint--top" data-hint="<?php echo __("Autor") ?>">	
								<a class="picto" href="<?php echo '/?author='.$my_post->post_author;?>">
									<i class="fa fa-user"></i>
								</a>					
							</div>
							<div class="circle_1  hint  hint--top" data-hint="<?php echo __("Open", "smc") ?>">
								<a class="picto" href="<?php the_permalink(); ?>">
									<i class="fa fa-folder-open"></i> 
								</a>
							</div>
							<div class="circle_1 hint  hint--top" data-hint="<?php echo __("Add Comment", "smc") ?>">
								<a class="picto" href="<?php comments_link(); ?>">
									<i class="fa fa-pencil"></i> 
								</a>
							</div>
						</div>
						<div style="border-bottom:1px dotted #EEE; ">
							<span class="post-atribute-autor">
								 <?php the_author_posts_link();  ?> <br>
							</span>
							<?php if($tax){?>
								<div class="post-attribute">
									<?php echo $tax; ?><br>
								</div>
							<?php }?>
							<?php if(has_category()){?>
								<div class="post-attribute">
									 <i class="fa fa-folder-open"></i> <?php the_category(',<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;') ?><br>
								</div>
							<?php }?>
							<div class="post-attribute">
								<i class="fa fa-envelope"></i> <?php echo direct_email(); ?>
							</div>
							<?php if (current_user_can('edit_post', $post->ID)) { ?>
								<div class="post-attribute">
										<i class="fa fa-pencil-square-o"></i> <?php edit_post_link('Правка', '', '');?><br>
								</div>
							<?php }?>
							<?php if( current_user_can( 'delete_posts' ) ) {
								//////echo '<div class="post-attribute"><a href="' . get_delete_post_link( 54, '', true) . '">Удалить без возможности восстановления</a></div>';
								}
							?>
						</div>
					</div>
				</td>
				<td valign='top' height='50' rowspan='2'>
					<DIV style="width:100%; display:inline-block;">
						<div style='padding-bottom:8px; border-bottom:0px solid #215584;width:100%;'>
							<div style="position:relative;">
								<h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Постоянная ссылка для <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
								<div   lang="ru"  style=" overflow-wrap: break-word!important; 
														  word-wrap: break-word!important;
														  word-break: normal; 
														  line-break: auto;
														  width:340px;">					
										<?php 
										$add_comment	= "оставить комментарий";
										?>	
										
									<p >				
										<?php the_content('<i class="fa fa-forward"></i>');  ?>
									</p>				
								</div>
								
								<!-- delete >
								<div>
									<a href="#inline-1" rel="wp-video-lightbox" >bebe</a>
									<div id="inline-1" style="display:none;">						
										
									</div>
								</div>
								<a href="/wp-content/themes/GamesStylish/location_map.php?iframe=true&width=500&height=800" rel="wp-video-lightbox[iframes]">map</a>
								<!-- end of delete -->
								
								
							</div>
						</div>
						<div style='width:100% word-wrap: break-word!important;'>
							<span><?php comments_popup_link($add_comment.'', $add_comment.'', $add_comment.'', 'add-comment');?></span>
							<span class="add-comment" style="background:#000;color:#888;">( <?php comments_number("0", "1", "%"); ?> )</span>
						</div >
					</DIV>
				</td>
			</tr>
			<tr>
				<td >
				</td>
			<tr>
			<!--tr >
				<td bgcolor=' #3686CB'style="height:1px">
				</td>
				<td bgcolor='#215584' style="height:1px">
				</td>
			<tr-->
		</table>
	</div>
	