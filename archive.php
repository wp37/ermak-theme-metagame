<?php 
		
		get_header(); 
		
		$type	= ""; 
		$post = $posts[0]; // Hack. Set $post so that the_date() works. 		   
		if (is_category()) 
		{ 
			/* If this is a category archive */
			$type	= "category";
		} 
		elseif( is_tag() ) 
		{
			/* If this is a tag archive */
			$type	= "tag";
		} 
		
		elseif (is_day()) 
		{ 
			/* If this is a daily archive */
			$type	= 'day';		
		} 
		elseif (is_month()) 
		{ 
			/* If this is a monthly archive */ 
			$type	= "month";
		} 
		elseif (is_year()) 
		{ 
			/* If this is a yearly archive */ 
			$type	= "year";			  
		} 
		elseif (is_author()) 
		{ 
			/* If this is an author archive */
			$type	= "author";
		}
		elseif (isset($_GET['paged']) && !empty($_GET['paged'])) 
		{
			/* If this is a paged archive */
			$type = "paged";			
		}
		else if(is_tax())
		{
			//$type = get_query_var('taxonomy');
			$type = 'tax';
		}
		function add_post_buttom()
		{
			?>
				<div>
					
				</div>
			<?php
		}
		?>
		
<div class="outer" id="contentwrap">
		
	
	<?php get_sidebars('left'); ?>
	<div class="postcont">
		<div id="content">	
		<?php
		switch($type)
		{
			case "category":
				?><h4 class="pagetitle"><?php single_cat_title(); ?></h4><?php
				break;
			case "tag":
				?><h4 class="pagetitle">Записи с меткой &#8216;<?php single_tag_title(); ?>&#8217;</h4><?php
				break;
			case "day":
				?><h4 class="pagetitle">Архив для <?php the_time('F j, Y'); ?></h4><?php
				break;
			case "month":
				?><h4 class="pagetitle">Архив для <?php the_time('F, Y'); ?></h4><?php
				break;
			case "year":
				?><h4 class="pagetitle">Архив для <?php the_time('Y'); ?></h4> <?php
				break;
			case "author":
				?><h4 class="pagetitle">Архив автора</h4><?php
				break;
			case "paged":
				?><h4 class="pagetitle">Архив блога</h4><?php
				break;
			case 'tax':
				?><h4 class="pagetitle"><i class="fa <?php echo get_logo($tax_id); ?> fa-lg"></i>&nbsp;<?php single_term_title(); ?></h4><?php
				break;
		}
		?>
		<?php if ( is_user_logged_in() && ($type=="category" || $type=="tax")) {
			echo apply_filters("smc_location_archive_title", single_cat_title("", false));
		?>
		
		
		<div class="entry">
			
		
			
		</div>
		<?php }?>
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php if( get_post_type() != "post") 
			{
				continue;
			}?>
			<div <?php post_class() ?>>
				<?php include( "post.php"); ?>
			</div>

		<?php endwhile; ?>
		
		<div class="navigation">
		<?php if (function_exists("pagination")) {
			pagination($additional_loop->max_num_pages);
} 		?>

		</div>
	<?php else :

		echo("<h2>Ничего не найдено.</h2>");
		get_search_form();
		get_template_part( 'content', '' );
		
		?>
		
	<?php endif; ?>
			</div>
		
			</div>

<?php get_sidebars('right'); ?>
	</div>
<?php get_footer(); ?>
