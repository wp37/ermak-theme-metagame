<?php
if(get_theme_option('featured_posts') != '') {
?>
<script type="text/javascript">
/*	function startGallery() {
		var myGallery = new gallery($('myGallery'), {
			timed: true,
			delay: 12000,
			slideInfoZoneOpacity: 0.8,
			showCarousel: false 
		});
	}
	window.addEvent('domready', startGallery);*/
</script>
<div class="featured-post-slider clearfix">
    
    <div class="featured-post-slides-container clearfix">
        
        <div class="featured-post-slides">		
            	<?php
				$featured_posts_category = get_theme_option('featured_posts_category');
				
				if($featured_posts_category != '' && $featured_posts_category != '0') 
				{
					global $post;
					/**/
					$arg				= array(
												'numberposts'     => 3,
												'offset'          => 0,
												'category'        => '',
												'orderby'         => 'post_date',
												'order'           => 'DESC',
												'include'         => '',
												'exclude'         => '',
												'meta_key'        => 'slide_1',
												'meta_value'      => '',
												'post_type'       => '',
												'post_mime_type'  => '',
												'post_parent'     => '',
												'post_status'     => 'publish'
											);
					 $featured_posts 	= get_posts($arg);
					 
					$arg1				= array(
												'numberposts'     => 8,
												'offset'          => 0,
												'category'        => '',
												'orderby'         => 'post_date',
												'order'           => 'DESC',
												'include'         => '',
												'exclude'         => '',
												'meta_key'        => 'youtube_id',
												'meta_value'      => '',
												'post_type'       => '',
												'post_mime_type'  => '',
												'post_parent'     => '',
												'post_status'     => 'publish'
											);
					$featured_posts1 	= get_posts($arg1);
					$featured_posts 	= array_merge( $featured_posts,  $featured_posts1);
					function cmd($a, $b) 
					{	
						if (strtotime ($a->post_date) == strtotime ($b->post_date)) 
						{
							return 0;
						}
						return (strtotime ($a->post_date) > strtotime ($b->post_date)) ? -1 : 1;
					}
					usort($featured_posts, "cmd");
					//print_r(strtotime ($featured_posts[0]->post_date));
					//die();
					
					/*
					$arg	= array(
										'meta_query' => array(
																'relation' => 'OR',
														array(
																'key' => 'youtube_id',
																'value' => ''
															  ),
														array(
																'key' => 'slide_1',
																'value' => ''
															  )
															),
										'orderby'    	=> 'post_date',
										'order'    		=> 'DESC',
										'post_status'    => 'publish'
									)
					$featured_posts 	= get_posts($arg);
					
					$args = array(
									
									'post_status'    => 'publish'
								  );
					$featured_posts 	= new WP_Query('cat=24');
					*/
					$i = 0;
					foreach($featured_posts as $post) 
					{
						if(!uamIsAccess()) continue; // UAM - see function.php/1612
					 	setup_postdata($post);
                        if ( version_compare( $wp_version, '2.9', '>=' ) ) {
                            $slide_image_full = get_the_post_thumbnail($post->ID,'large', array('class' => 'full'));
							if($slide_image_full == "")
								$slide_image_full = '<img src=http://february.metaversitet.tv/wp-content/uploads/2014/01/slide27.jpg>';;
                            $slide_image_thumbnail = get_the_post_thumbnail($post->ID,'large', array('class' => 'thumbnail'));
                        } else {
                            $get_slide_image = get_post_meta($post->ID, 'featured', true);
                            $slide_image_full = "<img src=\"$get_slide_image\" class=\"full\" alt=\"\" />";
                            $slide_image_thumbnail = "<img src=\"$get_slide_image\" class=\"thumbnail\" alt=\"\" />";
                        }
					 	
					  ?>
					  			  
					  
			         <div class="featured-post-slides-items">
                        <div class="featured-post-thumbnail">
                 			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="open">	
								<?php echo  $slide_image_full; ?>
							</a> 
						</div>                       
						<div class="featured-post-content-wrap">
							<?php 
							$youtube_id 		= get_post_meta($post->ID, 'youtube_id', true);
							$slide_title		= get_post_meta($post->ID, 'slide_title', true);
							$slide_1 			= get_post_meta($post->ID, 'slide_1', true);
							$slide_2 			= get_post_meta($post->ID, 'slide_2', true);
							$slide_3 			= get_post_meta($post->ID, 'slide_3', true);
							$slide_4 			= get_post_meta($post->ID, 'slide_4', true);
							preg_match('/google/', $slide_1, $matches);
							$is_google			= $matches[0];
							if($youtube_id)
							{
								/*echo '<div class="featured-post-video">
									<iframe width="320" height="240" src="//www.youtube.com/embed/A63k04620V8" frameborder="0" allowfullscreen></iframe>
								</div>';*/
								echo '<div class="featured-post-video">';
								echo  do_shortcode('[video_lightbox_youtube video_id="'.$youtube_id.'" width="640" height="480" auto_thumb="1"]');
								echo '</div>';
							}
							else if($slide_1)
							{
								echo '<div class="featured-post-slideshow"><div class="slideshow_thumb_cont">';
								if($is_google)
								{
									//$slide_title = " google";
									/*
									echo '<div class="slidshow_thumb_picto"><img src='.$slide_1.' style="display: inline"  title="'.$slide_title.'"></div>';	
									echo '<div style="position:absolute; top:10px; left:10px">';
											echo '<a href='.$slide_1.' target="_blank"><div style="position:relative;  float:left; margin:5px; padding:5px 10px; background:darkred;">1</a>';
										if($slide_2)
											echo '<a href='.$slide_2.' target="_blank"><div style="position:relative;  float:left; margin:5px; padding:5px 10px; background:darkred;">2</div></a>';		
										if($slide_3)
											echo '<a href='.$slide_3.' target="_blank"><div style="position:relative;  float:left; margin:5px; padding:5px 10px; background:darkred;">3</div></a>';		
										if($slide_4)
											echo '<a href='.$slide_4.' target="_blank"><div style="position:relative;  float:left; margin:5px; padding:5px 10px; background:darkred;">4</div></a>';	
									echo '</div>';
									*/
								}
								else
								{
									echo '<a href='.$slide_1.' rel="lightbox['.$slide_1.']">';
									echo '<div class="slidshow_thumb_picto"><img src='.$slide_1.' style="display: inline"  title="'.$slide_title.'"></div></a>';		
									if($slide_2)
										echo '<a href='.$slide_2.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb_empty"><img src='.$slide_2.' style="display: inline"  title="'.$slide_title.'"></div></a>';		
									if($slide_3)
										echo '<a href='.$slide_3.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb_empty"><img src='.$slide_3.' style="display: inline"  title="'.$slide_title.'"></div></a>';		
									if($slide_4)
										echo '<a href='.$slide_4.' rel="lightbox['.$slide_1.']"><div class="slidshow_thumb_empty"><img src='.$slide_4.' style="display: inline"  title="'.$slide_title.'"></div></a>';	
								}
								if($slide_title) 
									echo '<div style="position:absolute; bottom:10px;size:100px;margin:5px;padding:5px 10px;background:rgba(0,0,0,0.5);"><i class="fa fa-folder-open"></i>&nbsp;&nbsp;'.$slide_title.'</div>';
								echo '</a></div></div>';
							}
							?>
							<!--p>HELLO!</p-->
        					<div class="featured-post-content">
								<div class="featured-post-date"><?php the_category(', '); ?> ( <?php the_time('F j, Y'); ?>)</div>
							
                               <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">	<h3 class="featured-post-title"><?php the_title(); ?></h3>
									<?php the_excerpt(); ?>
									<?php //echo  $slide_image_thumbnail; ?>
								</a>
							</div>
						</div>
                     </div>
					<?php 
					}
					 
				} else {
				
					$contents		= array(
													array(
															"Современное СМИ - это прежде всего картинка!",
															"Все посты, которые содержат ссылки фото и рисованные истории, обязательно попадут на первую страницу. Откуда их сразу увидят все, заходящие в игру."),
													array(
															"Современное СМИ - это прежде всего движущаяся картинка!",
															"Все посты, которые содержат ссылки на Ваши ролики youtube обязательно попадут сюда. Если Вы хотите. чтобы Ваша информация распространилась быстрее и на болшее пространство - публикуйте видео."),
												);
					for($i = 1; $i <=count($contents); $i++) {
						?>
			         <div class="featured-post-slides-items">
                        <div class="featured-post-thumbnail">
                        	<a title="Это заголовок закрепленной записи <?php echo $i; ?>" href="#">		<img src="<?php bloginfo('template_directory'); ?>/jdgallery/slides/<?php echo $i; ?>.jpg" class="thumbnail" alt="" /> </a>
					</div>
                    <div class="featured-post-content-wrap">
        						<div class="featured-post-content">
									<h3 class="featured-post-title"><a href="#"><?php echo $contents[$i][0]; ?></a></h3>
									<span class="featured-post-title"><?php echo $contents[$i][1]; ?></span>
							
							</div>
                            </div>
                            
                            </div>
						<?php
					}
					/**/
				}
				
				?>
		
		</div>  		
		
		<!--div class="featured-post-slides" style="font-size:40px;">
				что
		</div-->  
		 <div class="featured-post-prev-next-wrap">
			<!--div class="featured-post-prev-next"-->
				<a href="#featured-post-next" class="featured-post-next"><i class="fa fa-angle-right fa-4x"></i> </a>
				<a href="#featured-post-prev" class="featured-post-prev"><i class="fa fa-angle-left fa-4x"></i> </a>
			<!--/div-->  
			<div class='featured-instruction'>
				сюда попадают все  <b><a href="/?page_id=1280">фотки</a> и <a href="/?page_id=1283">ролики</a></b><!--чтобы попасть сюда, надо опубликовать видео или комикс-->
			</div>	
		</div>   
		 <div class="featured-post-nav">
			<span class="featured-post-pager">&nbsp;</span>
		</div> 
	</div> 
</div>   
<script>
$j=jQuery.noConflict();
$j(document).ready(function(){
	jQuery('.featured-post-slides').cycle({
		fx: 'scrollHorz',
		timeout: 16000,
		delay: 0,
		speed: 1200,
		next: '.featured-post-next',
		prev: '.featured-post-prev',
		pager: '.featured-post-pager',
		continuous: 0,
		sync: 1,
		pause: 1,
		pauseOnPagerHover: 1,
		cleartype: true,
		cleartypeNoBg: true
	});
 });
</script>

<?php } wp_reset_query();?>